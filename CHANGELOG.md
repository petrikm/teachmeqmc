
Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2021-03-06
  * The program is able to:
      - define a Boolean function,
      - utilize the Quine-McCluskey algorithm to find all minimal DNF of
        such a function (however, when the covering matrix cannot be
        simplified any firther, brute force method is applied),
      - export a report about the performing of the Quine-McCluskey
        algorithm both in plain text and in LaTeX code,
      - alter the given Boolean function by negating and permutating the
        literals in the complete DNF to obtain differently looking yet same
        exercises on the Quine-McCluskey algorithm.

[0.1.0]: https://gitlab.com/petrikm/teachmeqmc/-/tags/0.1.0

