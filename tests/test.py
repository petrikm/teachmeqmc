# -*- coding: UTF-8 -*-

from unittest import TestCase

from teachmeqmc import BooleanFunction

class TestTeachMeQMC(TestCase):

    def defineDefaultBooleanFunction(self):
        """
        Defines the Boolean function for which the following tests are run.
        """
        f = BooleanFunction(4)
        f.addTerm((0, 0, 0, 0))
        f.addTerm((0, 0, 0, 1))
        f.addTerm((0, 0, 1, 0))
        f.addTerm((0, 0, 1, 1))
        f.addTerm((0, 1, 0, 0))
        f.addTerm((1, 1, 0, 0))
        f.addTerm((1, 1, 0, 1))
        f.addTerm((1, 0, 0, 1))
        f.addTerm((1, 0, 0, 1))
        return f

    def test_cdnf_to_text(self):
        """
        Compares the initial complete DNF converted to plain text with the
        expected value.
        """
        f = self.defineDefaultBooleanFunction()
        expectedValue = "f(a,b,c,d) = a'b'c'd' + a'b'c'd + a'b'cd' + a'b'cd + a'bc'd' + abc'd' \n+ abc'd + ab'c'd + ab'c'd \n"
        self.assertEqual(f.exportCDNFToText(), expectedValue)

    def test_cdnf_to_latex(self):
        """
        Compares the initial complete DNF converted to LaTeX with the
        expected value.
        """
        f = self.defineDefaultBooleanFunction()
        expectedValue = r"$\varphi{\equiv}$  $\left(a'{\land}b'{\land}c'{\land}d'\right)$  ${\lor}$  $\left(a'{\land}b'{\land}c'{\land}d\right)$  ${\lor}$  $\left(a'{\land}b'{\land}c{\land}d'\right)$  ${\lor}$  $\left(a'{\land}b'{\land}c{\land}d\right)$  ${\lor}$  $\left(a'{\land}b{\land}c'{\land}d'\right)$  ${\lor}$  $\left(a{\land}b{\land}c'{\land}d'\right)$  ${\lor}$  $\left(a{\land}b{\land}c'{\land}d\right)$  ${\lor}$  $\left(a{\land}b'{\land}c'{\land}d\right)$  ${\lor}$  $\left(a{\land}b'{\land}c'{\land}d\right)$ "
        self.assertEqual(f.exportCDNFToLaTeX(), expectedValue)

    def test_result(self):
        """
        Compares the resulting minimized DNF saved in the attributes of
        `teachmeqmc.BooleanFunction` with the expected values.
        """
        f = self.defineDefaultBooleanFunction()
        f.performQuineMcCluskey()
        self.assertEqual(len(f.result), 1)
        assumed = [(0, 0, 2, 2), (2, 1, 0, 0), (1, 2, 0, 1)]
        for i in range(len(assumed)):
            self.assertEqual(f.result[0][i].term, assumed[i])

    def test_result_to_text(self):
        """
        Compares the resulting minimized DNF converted to plain text with the
        expected value.
        """
        f = self.defineDefaultBooleanFunction()
        f.performQuineMcCluskey()
        expectedValue = "f(a,b,c,d) = a'b' + bc'd' + ac'd \n"
        self.assertEqual(f.exportResultToText(), expectedValue)

    def test_result_to_latex(self):
        """
        Compares the resulting minimized DNF converted to LaTeX with the
        expected value.
        """
        f = self.defineDefaultBooleanFunction()
        f.performQuineMcCluskey()
        expectedValue = r"$\varphi{\equiv}\left(a'{\land}b'\right){\lor}\left(b{\land}c'{\land}d'\right){\lor}\left(a{\land}c'{\land}d\right)$"
        self.assertEqual(f.exportResultToLaTeX(), expectedValue)

if __name__ == '__main__':
    unittest.main()

