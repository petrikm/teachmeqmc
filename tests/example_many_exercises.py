# -*- coding: UTF-8 -*-

# ==========================================================================
#
# Note that the package teachmeqmc needs to be installed before running this
# program.
# This can be done e.g. by using the Python virtual environment.
# Change the current directory to the root of this project (the directory where
# setup.py is placed).
# Run the commands:
#
#   python3 -m venv ve
#   source ve/bin/activate
#   python3 -m pip install -e .
#
# To run this program:
#
#   cd tests
#   python3 this_program.py
#
# The virtual environment can be closed by typing:
#
#   deactivate
#
# ==========================================================================

import datetime

from teachmeqmc import BooleanFunction
from teachmeqmc import getLaTeXHead
from teachmeqmc import getLaTeXFoot

indentTab = "    "

exercises = []

qmc = BooleanFunction(1)
qmc.setComment("Tautologie")
qmc.addTerm((0,))
exercises.append(qmc)

qmc = BooleanFunction(2)
qmc.setComment("Tautologie")
qmc.addTerm((0, 0))
qmc.addTerm((0, 1))
qmc.addTerm((1, 0))
qmc.addTerm((1, 1))
exercises.append(qmc)

qmc = BooleanFunction(2)
qmc.setComment("Implikace")
qmc.addTerm((0, 0))
qmc.addTerm((0, 1))
qmc.addTerm((1, 1))
exercises.append(qmc)

qmc = BooleanFunction(3)
qmc.setComment("Typ 1: 4 vst. termy, 1 výsledek, po vyškrtání jednoznačných vypadne jeden výsledný term")
qmc.addTerm((0, 0, 1))
qmc.addTerm((0, 1, 0))
qmc.addTerm((0, 1, 1))
qmc.addTerm((1, 1, 0))
exercises.append(qmc)

qmc = BooleanFunction(4)
qmc.setComment("Typ 81: 8 vst. termů, 1 výsledek, dominance sloupců")
qmc.addTerm((0, 0, 0, 0))
qmc.addTerm((0, 0, 0, 1))
qmc.addTerm((0, 1, 0, 1))
qmc.addTerm((0, 1, 1, 1))
qmc.addTerm((1, 1, 1, 0))
qmc.addTerm((1, 1, 1, 1))
qmc.addTerm((1, 0, 1, 0))
qmc.addTerm((1, 0, 1, 1))
exercises.append(qmc)

qmc = BooleanFunction(4)
qmc.setComment("Typ 85: 8 vst. termů, 1 výsledek, dominance řádků i sloupců")
qmc.addTerm((0, 0, 0, 0))
qmc.addTerm((0, 0, 0, 1))
qmc.addTerm((0, 0, 1, 0))
qmc.addTerm((0, 0, 1, 1))
qmc.addTerm((0, 1, 0, 0))
qmc.addTerm((1, 1, 0, 0))
qmc.addTerm((1, 1, 0, 1))
qmc.addTerm((1, 0, 0, 1))
exercises.append(qmc)

qmc = BooleanFunction(4)
qmc.setComment("Typ 101: 10 vst. termů, 3 výsledky, dominance řádků (příklad ze skript Logické systémy)")
qmc.addTerm((0, 0, 0, 1))
qmc.addTerm((0, 1, 0, 0))
qmc.addTerm((0, 1, 1, 1))
qmc.addTerm((1, 0, 0, 0))
qmc.addTerm((1, 0, 0, 1))
qmc.addTerm((1, 0, 1, 0))
qmc.addTerm((1, 0, 1, 1))
qmc.addTerm((1, 1, 0, 0))
qmc.addTerm((1, 1, 1, 0))
qmc.addTerm((1, 1, 1, 1))
exercises.append(qmc)

qmc = BooleanFunction(5)
qmc.setComment("Typ 101: 10 vst. termů, 3 výsledky, dominance řádků (příklad ze skript Logické systémy)")
qmc.addTerm((0, 0, 0, 0, 1))
qmc.addTerm((0, 1, 0, 0, 0))
qmc.addTerm((0, 1, 0, 1, 1))
qmc.addTerm((1, 0, 0, 0, 0))
qmc.addTerm((1, 0, 0, 0, 1))
qmc.addTerm((1, 0, 0, 1, 0))
qmc.addTerm((1, 0, 0, 1, 1))
qmc.addTerm((1, 1, 0, 0, 0))
qmc.addTerm((1, 1, 0, 1, 0))
qmc.addTerm((1, 1, 0, 1, 1))
exercises.append(qmc)

qmc = BooleanFunction(6)
qmc.setComment("Typ 101: 10 vst. termů, 3 výsledky, dominance řádků (příklad ze skript Logické systémy)")
qmc.addTerm((0, 0, 0, 0, 0, 1))
qmc.addTerm((0, 0, 1, 0, 0, 0))
qmc.addTerm((0, 0, 1, 0, 1, 1))
qmc.addTerm((0, 1, 0, 0, 0, 0))
qmc.addTerm((0, 1, 0, 0, 0, 1))
qmc.addTerm((0, 1, 0, 0, 1, 0))
qmc.addTerm((0, 1, 0, 0, 1, 1))
qmc.addTerm((0, 1, 1, 0, 0, 0))
qmc.addTerm((0, 1, 1, 0, 1, 0))
qmc.addTerm((0, 1, 1, 0, 1, 1))
exercises.append(qmc)

qmc = BooleanFunction(7)
qmc.setComment("Typ 101: 10 vst. termů, 3 výsledky, dominance řádků (příklad ze skript Logické systémy)")
qmc.addTerm((0, 0, 0, 0, 0, 0, 1))
qmc.addTerm((0, 0, 0, 1, 0, 0, 0))
qmc.addTerm((0, 0, 0, 1, 0, 1, 1))
qmc.addTerm((0, 0, 1, 0, 0, 0, 0))
qmc.addTerm((0, 0, 1, 0, 0, 0, 1))
qmc.addTerm((0, 0, 1, 0, 0, 1, 0))
qmc.addTerm((0, 0, 1, 0, 0, 1, 1))
qmc.addTerm((0, 0, 1, 1, 0, 0, 0))
qmc.addTerm((0, 0, 1, 1, 0, 1, 0))
qmc.addTerm((0, 0, 1, 1, 0, 1, 1))
exercises.append(qmc)

qmc = BooleanFunction(4)
qmc.addTerm((0, 0, 0, 0))
qmc.addTerm((0, 0, 1, 1))
qmc.addTerm((0, 1, 0, 1))
qmc.addTerm((0, 1, 1, 0))
qmc.addTerm((1, 1, 0, 0))
qmc.addTerm((1, 1, 1, 1))
qmc.addTerm((1, 0, 0, 1))
qmc.addTerm((1, 0, 1, 0))
exercises.append(qmc)

qmc = BooleanFunction(5)
qmc.addTerm((0, 0, 0, 0, 0))
qmc.addTerm((0, 0, 0, 1, 1))
qmc.addTerm((0, 0, 1, 0, 1))
qmc.addTerm((0, 0, 1, 1, 0))
qmc.addTerm((0, 1, 1, 0, 0))
qmc.addTerm((0, 1, 1, 1, 1))
qmc.addTerm((0, 1, 0, 0, 1))
qmc.addTerm((0, 1, 0, 1, 0))
qmc.addTerm((1, 1, 0, 0, 0))
qmc.addTerm((1, 1, 0, 1, 1))
qmc.addTerm((1, 1, 1, 0, 1))
qmc.addTerm((1, 1, 1, 1, 0))
qmc.addTerm((1, 0, 1, 0, 0))
qmc.addTerm((1, 0, 1, 1, 1))
qmc.addTerm((1, 0, 0, 0, 1))
qmc.addTerm((1, 0, 0, 1, 0))
exercises.append(qmc)

qmc = BooleanFunction(6)
qmc.addTerm((0, 0, 0, 0, 0, 0))
qmc.addTerm((0, 0, 0, 0, 1, 1))
qmc.addTerm((0, 0, 0, 1, 0, 1))
qmc.addTerm((0, 0, 0, 1, 1, 0))
qmc.addTerm((0, 0, 1, 1, 0, 0))
qmc.addTerm((0, 0, 1, 1, 1, 1))
qmc.addTerm((0, 0, 1, 0, 0, 1))
qmc.addTerm((0, 0, 1, 0, 1, 0))
qmc.addTerm((0, 1, 1, 0, 0, 0))
qmc.addTerm((0, 1, 1, 0, 1, 1))
qmc.addTerm((0, 1, 1, 1, 0, 1))
qmc.addTerm((0, 1, 1, 1, 1, 0))
qmc.addTerm((0, 1, 0, 1, 0, 0))
qmc.addTerm((0, 1, 0, 1, 1, 1))
qmc.addTerm((0, 1, 0, 0, 0, 1))
qmc.addTerm((0, 1, 0, 0, 1, 0))

qmc.addTerm((1, 1, 0, 0, 0, 0))
qmc.addTerm((1, 1, 0, 0, 1, 1))
qmc.addTerm((1, 1, 0, 1, 0, 1))
qmc.addTerm((1, 1, 0, 1, 1, 0))
qmc.addTerm((1, 1, 1, 1, 0, 0))
qmc.addTerm((1, 1, 1, 1, 1, 1))
qmc.addTerm((1, 1, 1, 0, 0, 1))
qmc.addTerm((1, 1, 1, 0, 1, 0))
qmc.addTerm((1, 0, 1, 0, 0, 0))
qmc.addTerm((1, 0, 1, 0, 1, 1))
qmc.addTerm((1, 0, 1, 1, 0, 1))
qmc.addTerm((1, 0, 1, 1, 1, 0))
qmc.addTerm((1, 0, 0, 1, 0, 0))
qmc.addTerm((1, 0, 0, 1, 1, 1))
qmc.addTerm((1, 0, 0, 0, 0, 1))
qmc.addTerm((1, 0, 0, 0, 1, 0))
exercises.append(qmc)

fileName = "out_many_exercises"

print("Writing file:", fileName + ".tex")
with open(fileName + ".tex", "w") as out: 
    out.write(getLaTeXHead())
    for i in range(len(exercises)):
        qmc = exercises[i]
        qmc.performQuineMcCluskey()
        out.write(qmc.exportToLaTeX(
            name = "\\varphi",
            textOr = "+",
            textAnd = "",
            parentheses = False,
            letters = None,
            indent = "",
            indentTab = "    "
            ))
    out.write("\n")
    out.write(r'\end{document}' + "\n")

print("Writing file:", fileName + ".txt")
with open(fileName + ".txt", "w") as out: 
    for i in range(len(exercises)):
        qmc = exercises[i]
        out.write(qmc.exportToText())
        out.write("\n")
        out.write("\n")
        out.write(" ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ## \n")
        out.write(" ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ##  ## \n")
        out.write("\n")
        out.write("\n")
    

