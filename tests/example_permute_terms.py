from teachmeqmc import BooleanFunction

f = BooleanFunction(3)
f.addTerm((0, 0, 0))
f.addTerm((0, 0, 1))
f.addTerm((0, 1, 1))
f.addTerm((1, 1, 0))
f.addTerm((1, 1, 1))

permutations = []
permutations.append((0, 1, 2))
permutations.append((1, 2, 0))
permutations.append((2, 0, 1))
permutations.append((0, 2, 1))

exercises = []
for perm in permutations:
    exercise = f.getCopy()
    exercise.permuteTerms(perm)
    exercise.duplicateTerms(8)
    exercise.shuffleTerms()
    exercises.append(exercise)

for i in range(len(exercises)):
    exercise = exercises[i]
    exercise.performQuineMcCluskey()
    exercise.saveToLaTeXFile("exercise_perm_" + str(i) + ".tex")
