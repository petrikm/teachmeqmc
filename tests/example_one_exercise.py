# -*- coding: UTF-8 -*-

# ==========================================================================
#
# Note that the package teachmeqmc needs to be installed before running this
# program.
# This can be done e.g. by using the Python virtual environment.
# Change the current directory to the root of this project (the directory where
# setup.py is placed).
# Run the commands:
#
#   python3 -m venv ve
#   source ve/bin/activate
#   python3 -m pip install -e .
#
# To run this program:
#
#   cd tests
#   python3 this_program.py
#
# The virtual environment can be closed by typing:
#
#   deactivate
#
# ==========================================================================

from teachmeqmc import BooleanFunction

f = BooleanFunction(4)

f.setComment("Typ 85: 8 vst. termů, 1 výsledek, dominance řádků i sloupců")

f.addTerm((0, 0, 0, 0))
f.addTerm((0, 0, 0, 1))
f.addTerm((0, 0, 1, 0))
f.addTerm((0, 0, 1, 1))
f.addTerm((0, 1, 0, 0))
f.addTerm((1, 1, 0, 0))
f.addTerm((1, 1, 0, 1))
f.addTerm((1, 0, 0, 1))
f.addTerm((1, 0, 0, 1))

f.performQuineMcCluskey()

fileName = "out_one_exercise"

print("Writing file", fileName + ".tex")
f.saveToLaTeXFile(fileName + ".tex")

print("Writing file", fileName + ".txt")
f.saveToTextFile(fileName + ".txt")
