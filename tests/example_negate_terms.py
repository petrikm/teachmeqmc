from teachmeqmc import BooleanFunction

f = BooleanFunction(3)
f.addTerm((0, 0, 0))
f.addTerm((0, 0, 1))
f.addTerm((0, 1, 1))
f.addTerm((1, 1, 0))
f.addTerm((1, 1, 1))

masks = []
masks.append((0, 0, 0))
masks.append((0, 0, 1))
masks.append((0, 1, 0))
masks.append((0, 1, 1))

exercises = []
for mask in masks:
    exercise = f.getCopy()
    exercise.negateTerms(mask)
    exercise.duplicateTerms(8)
    exercise.shuffleTerms()
    exercises.append(exercise)

for i in range(len(exercises)):
    exercise = exercises[i]
    exercise.performQuineMcCluskey()
    exercise.saveToLaTeXFile("exercise_neg_" + str(i) + ".tex")
